License: GPLv3

A quick & dirty items.xml edit to balance and buff some underpowered active items. I don't recommend using this with other mods as it may overide custom items.
